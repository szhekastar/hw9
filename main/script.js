function openClass (event, nameLi = '') {

    console.log(event);
    let content = document.getElementsByClassName('content');
    let tabsTitle = document.getElementsByClassName('tabs-title');
    for(let i = 0; i < content.length; i++) {
        content[i].style.display = 'none';
        tabsTitle[i].classList.remove('active');
    }
    document.getElementById(nameLi).style.display = 'block';

    event.target.classList.add("active");
}


